#import GPIO
import RPi.GPIO as GPIO
from time import sleep

#set GPIO mode
GPIO.setmode(GPIO.BCM)

#setting up motor controlling pins
GPIO.setup(17,GPIO.OUT) #pin A
GPIO.setup(27,GPIO.OUT) #pin B
GPIO.setup(19,GPIO.OUT) #pin C
GPIO.setup(26,GPIO.OUT) #pin D


def forward():
    GPIO.output(17,True)
    GPIO.output(27,False)
    GPIO.output(19,False)
    GPIO.output(26,True)

def stop():
    GPIO.output(17,False)
    GPIO.output(27,False)
    GPIO.output(19,False)
    GPIO.output(26,False)

def right():
    GPIO.output(17,False)
    GPIO.output(27,True)
    GPIO.output(19,False)
    GPIO.output(26,True)

def left():
    GPIO.output(17,True)
    GPIO.output(27,False)
    GPIO.output(19,True)
    GPIO.output(26,False)


try:
    while True:
        forward()
        print("forward")
        sleep(0.5)
        backward()
        print("backward")
        sleep(0.5)
        right()
        print("right")
        sleep(0.5)
        left()
        print("left")
        sleep(0.5)


finally:
    GPIO.cleanup()